package test.usage;

import com.github.msemys.esjc.RecordedEvent;
import com.google.gson.Gson;

import lombok.RequiredArgsConstructor;
import test.eventstore.EventStream;
import test.eventstore.GsonEventHandler;
import test.eventstore.TransientStreamListener;

@EventStream(streamId = "_foo")
@RequiredArgsConstructor
public class FooExampleListener implements TransientStreamListener {
	final Gson gson;

	@GsonEventHandler(type = "bar")
	public void handle(SomePojo b) {
		System.out.println("Recieved type bar: " + b);
	}

	@Override
	public void handleUnknownEvent(RecordedEvent originalEvent) {
		System.out.println("unknown " + gson.toJson(originalEvent));
	}

	@Override
	public void notifyCatchup() {
		System.err.println("catchup! " + this.getClass().getSimpleName());
	}

}
