package test.usage;

import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;

import com.github.msemys.esjc.RecordedEvent;
import com.google.common.io.Files;
import com.google.gson.Gson;

import lombok.Data;
import test.eventstore.EventStream;
import test.eventstore.GsonEventHandler;
import test.eventstore.PersistentStreamListener;

@EventStream(streamId = "foo")

public class PersistentFooExampleListener implements PersistentStreamListener {

	private final Gson gson;
	private final File file;
	private final FooExampleState state;

	public PersistentFooExampleListener(Gson gson) {
		this.gson = gson;
		this.file = new File("/tmp/ptest");
		state = loadStateFromDisk();
	}

	@GsonEventHandler(type = "bar")
	public synchronized void handle(SomePojo b, RecordedEvent e) {
		state.count++;
		state.lastProcessed = e.eventNumber;
		persistStateToDisk();
		System.out.println("pbar type bar: " + b + " " + e.eventNumber);
	}

	@Override
	public Integer getLastEventNumber() {
		return state.lastProcessed;
	}

	private void persistStateToDisk() {
		try {
			Files.write(gson.toJson(state), file, Charset.defaultCharset());
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
	}

	private FooExampleState loadStateFromDisk() {
		try {
			if (file.exists()) {
				String json = Files.readFirstLine(file, Charset.defaultCharset());
				FooExampleState fromJson = gson.fromJson(json, FooExampleState.class);
				System.out.println("Loaded state from disk: " + json);
				return fromJson;
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return new FooExampleState();
	}

	@Data
	public static class FooExampleState {
		Integer lastProcessed = null;
		int count = 0;
	}
}
