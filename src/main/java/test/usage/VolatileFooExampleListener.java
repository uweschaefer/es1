package test.usage;

import com.github.msemys.esjc.RecordedEvent;

import test.eventstore.EventStream;
import test.eventstore.GsonEventHandler;
import test.eventstore.VolatileStreamListener;

@EventStream(streamId = "foo")
public class VolatileFooExampleListener implements VolatileStreamListener {

	@GsonEventHandler(type = "bar")
	public synchronized void handle(SomePojo b, RecordedEvent e) {
		System.out.println("vbar type bar: " + b + " " + e.eventNumber);
	}
}
