package test.usage;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class SomePojo {
	String someString;
	int someInt = -1;
}
