package test.usage;

import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import com.github.msemys.esjc.EventStore;

import lombok.RequiredArgsConstructor;
import test.eventstore.EventStoreGsonPublisher;

@Component
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class CmdLine implements CommandLineRunner {

	private static final int MAX = 100_000;
	final EventStore eventstore;
	final EventStoreGsonPublisher pub;

	@Override
	public void run(String... arg0) throws Exception {

		String streamName = "foo";

		pub.publishPojo(streamName, "bar",
				SomePojo.builder().someInt((int) (Math.random() * 100)).someString("heyho").build());

		pub.publishJson("baz", "bar", "{ \"someString\":\"manual\"}").get();

		long start = System.currentTimeMillis();

		List<String> prepared = new LinkedList<>();

		for (int i = 0; i < CmdLine.MAX; i++) {
			prepared.add("{ \"someString\":\"gurke\"}");
		}

		prepared.stream().map(s -> pub.publishJson("sink", "bar", s)).collect(Collectors.toList()).forEach(cf -> {
			try {
				cf.get();
			} catch (InterruptedException | ExecutionException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		});

		System.out.println("rt=" + (System.currentTimeMillis() - start));

		Thread.sleep(10 * 1000);
		eventstore.disconnect();
	}

}
