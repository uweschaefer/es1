package test.usage;

import java.util.concurrent.atomic.AtomicInteger;

import com.google.gson.Gson;

import lombok.RequiredArgsConstructor;
import test.eventstore.EventStream;
import test.eventstore.GsonEventHandler;
import test.eventstore.TransientStreamListener;

@EventStream(streamId = "_baz")
@RequiredArgsConstructor
public class BazExampleListener implements TransientStreamListener {
	final Gson gson;
	private AtomicInteger count = new AtomicInteger(0);

	@GsonEventHandler(type = "bar")
	public void handle(SomePojo b) {
		System.out.println("Recieved: " + b);
	}
}
