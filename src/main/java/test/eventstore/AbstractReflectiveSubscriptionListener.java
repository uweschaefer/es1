package test.eventstore;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.util.ReflectionUtils;

import com.github.msemys.esjc.RecordedEvent;
import com.github.msemys.esjc.ResolvedEvent;
import com.google.gson.Gson;

public class AbstractReflectiveSubscriptionListener<T extends StreamListener> implements StreamListener {
	protected final T target;
	protected final Gson gson;

	public AbstractReflectiveSubscriptionListener(T target, Gson gson) {
		this.target = target;
		this.gson = gson;
		this.clazz = target.getClass();
		init();
	}

	interface GsonMethodIvocation {
		void invoke(Object pojo, ResolvedEvent e)
				throws IllegalAccessException, IllegalArgumentException, InvocationTargetException;
	}

	protected final Class<? extends Object> clazz;
	protected final Map<String, Class<?>> gsonMappings = new HashMap<>();
	protected final Map<String, GsonMethodIvocation> gsonMethods = new HashMap<>();

	private void init() {
		List<Method> methods = Arrays.asList(ReflectionUtils.getAllDeclaredMethods(clazz));
		methods.stream().filter(m -> m.getAnnotation(GsonEventHandler.class) != null).forEach(m -> {
			gsonMappings.put(m.getAnnotation(GsonEventHandler.class).type(), m.getParameterTypes()[0]);
			gsonMethods.put(m.getAnnotation(GsonEventHandler.class).type(), createMethodInvocation(m));
		});
	}

	private GsonMethodIvocation createMethodInvocation(Method m) {
		if (m.getParameterTypes().length == 2) {
			if (m.getParameterTypes()[1] == RecordedEvent.class) {
				return (o, e) -> m.invoke(target, o, e.originalEvent());
			} else {
				// TODO
				throw new RuntimeException("Don't know how to invoke " + m);
			}
		} else {
			return (o, e) -> m.invoke(target, o);
		}
	}

	public void invokeTargetMethod(ResolvedEvent event) {
		String type = event.originalEvent().eventType;
		Class<?> gsonDeserTarget = gsonMappings.get(type);
		if (gsonDeserTarget != null) {
			Object targetParam = gson.fromJson(new String(event.originalEvent().data), gsonDeserTarget);
			try {
				gsonMethods.get(type).invoke(targetParam, event);
			} catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		} else {
			target.handleUnknownEvent(event.originalEvent());
		}

	}

}
