package test.eventstore;

import org.springframework.boot.context.properties.ConfigurationProperties;

import lombok.AccessLevel;
import lombok.Data;
import lombok.experimental.FieldDefaults;

@ConfigurationProperties(prefix = "eventstore")
@Data
@FieldDefaults(level = AccessLevel.PRIVATE)
public class EventStoreConfigurationProperties {
	String userName = "admin";
	String password = "changeit";
	String host = "127.0.0.1";
	int port = 1113;

}
