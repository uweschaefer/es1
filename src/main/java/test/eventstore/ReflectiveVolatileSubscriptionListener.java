package test.eventstore;

import com.github.msemys.esjc.ResolvedEvent;
import com.github.msemys.esjc.Subscription;
import com.github.msemys.esjc.VolatileSubscriptionListener;
import com.google.gson.Gson;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class ReflectiveVolatileSubscriptionListener
		extends AbstractReflectiveSubscriptionListener<VolatileStreamListener> implements VolatileSubscriptionListener {

	public ReflectiveVolatileSubscriptionListener(VolatileStreamListener target, Gson gson) {
		super(target, gson);
	}

	@Override
	public void onEvent(Subscription arg0, ResolvedEvent arg1) {
		invokeTargetMethod(arg1);
	}
}
