package test.eventstore;

import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

import com.github.msemys.esjc.EventStore;
import com.github.msemys.esjc.EventStoreBuilder;

@Configuration
@EnableConfigurationProperties(EventStoreConfigurationProperties.class)
@Import(EventStoreListenerConfiguration.class)
public class EventStoreConfiguration {

	@Bean
	public EventStore eventStore(EventStoreConfigurationProperties p) {
		EventStore es = EventStoreBuilder.newBuilder().userCredentials(p.getUserName(), p.getPassword())
				.singleNodeAddress(p.getHost(), p.getPort()).build();
		es.connect();
		return es;
	}

}
