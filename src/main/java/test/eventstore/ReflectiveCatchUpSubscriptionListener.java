package test.eventstore;

import com.github.msemys.esjc.CatchUpSubscription;
import com.github.msemys.esjc.CatchUpSubscriptionListener;
import com.github.msemys.esjc.ResolvedEvent;
import com.github.msemys.esjc.SubscriptionDropReason;
import com.google.gson.Gson;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class ReflectiveCatchUpSubscriptionListener<T extends CatchupStreamListener>
		extends AbstractReflectiveSubscriptionListener<T> implements CatchUpSubscriptionListener {

	public ReflectiveCatchUpSubscriptionListener(T target, Gson gson) {
		super(target, gson);
	}

	@Override
	public void onLiveProcessingStarted(CatchUpSubscription subscription) {
		target.notifyCatchup();
	}

	@Override
	public void onEvent(CatchUpSubscription subscription, ResolvedEvent event) {
		invokeTargetMethod(event);
	}

	@Override
	public void onClose(CatchUpSubscription subscription, SubscriptionDropReason reason, Exception exception) {
		ReflectiveCatchUpSubscriptionListener.log.warn("Subscription " + subscription + " closed. Reason: {} ", reason);
	}
}
