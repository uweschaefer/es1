package test.eventstore;

public interface PersistentStreamListener extends CatchupStreamListener {

	Integer getLastEventNumber();

	@Override
	default void notifyCatchup() {
	}
}
