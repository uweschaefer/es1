package test.eventstore;

import java.util.Arrays;
import java.util.stream.Collectors;

import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.context.annotation.Configuration;

import com.github.msemys.esjc.EventStore;
import com.google.gson.Gson;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Configuration
public class EventStoreListenerConfiguration implements ApplicationContextAware {

	@Override
	public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {

		EventStore es = applicationContext.getBean(EventStore.class);
		Gson gson = applicationContext.getBean(Gson.class);

		Arrays.asList(applicationContext.getBeanNamesForAnnotation(EventStream.class)).forEach(name -> {

			Object bean = applicationContext.getBean(name);
			Class<? extends Object> clazz = bean.getClass();
			String streamName = clazz.getAnnotation(EventStream.class).streamId();

			if (bean instanceof PersistentStreamListener) {
				EventStoreListenerConfiguration.log.info("Subscribing PersistentStreamListener '" + name + "'");
				PersistentStreamListener pl = (PersistentStreamListener) bean;

				es.subscribeToStreamFrom(streamName, pl.getLastEventNumber(),
						new ReflectiveCatchUpSubscriptionListener<>(pl, gson));
				return;
			}

			if (bean instanceof TransientStreamListener) {
				EventStoreListenerConfiguration.log.info("Subscribing TransientStreamListener '" + name + "'");
				TransientStreamListener esl = (TransientStreamListener) bean;
				// TODO result
				es.subscribeToStreamFrom(streamName, null, new ReflectiveCatchUpSubscriptionListener<>(esl, gson));
				return;
			}

			if (bean instanceof VolatileStreamListener) {
				EventStoreListenerConfiguration.log.info("Subscribing VolatileStreamListener '" + name + "'");
				VolatileStreamListener pl = (VolatileStreamListener) bean;

				es.subscribeToStream(streamName, true, new ReflectiveVolatileSubscriptionListener(pl, gson));
				return;
			}

			throw new RuntimeException("@EventStream-annotated classes must implement one of " + Arrays
					.asList(new Class[] { VolatileStreamListener.class, TransientStreamListener.class,
							PersistentStreamListener.class })
					.stream().map(c -> c.getSimpleName()).collect(Collectors.joining(", ")));

		}

		);
	}
}
