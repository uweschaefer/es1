package test.eventstore;

public interface CatchupStreamListener extends StreamListener {

	default void notifyCatchup() {
	}
}
