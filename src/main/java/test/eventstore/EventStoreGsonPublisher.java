package test.eventstore;

import java.util.concurrent.CompletableFuture;

import org.springframework.stereotype.Service;

import com.github.msemys.esjc.EventData;
import com.github.msemys.esjc.EventStore;
import com.github.msemys.esjc.ExpectedVersion;
import com.github.msemys.esjc.WriteResult;
import com.google.gson.Gson;

import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor
public class EventStoreGsonPublisher {
	final EventStore es;
	final Gson gson;

	public CompletableFuture<WriteResult> publishPojo(String streamName, String type, Object pojo) {
		return publishJson(streamName, type, gson.toJson(pojo));
	}

	public CompletableFuture<WriteResult> publishJson(String streamName, String type, String json) {
		return es.appendToStream(streamName, ExpectedVersion.ANY,
				EventData.newBuilder().type(type).jsonData(json).build());
	}
}
