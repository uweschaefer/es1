package test.eventstore;

import com.github.msemys.esjc.RecordedEvent;

public interface StreamListener {
	default void handleUnknownEvent(RecordedEvent originalEvent) {
	}
}
